module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        babel: {
            options: {
                sourceMap: false,
                presets: ['es2015-without-strict']
            },
            dist: {
                expand: true,
                cwd: 'src/es6',
                src: ['*.js'],
                dest: 'tmp/js/'
            }
        },
        concat: {
            build: {
                options: {
                    banner: '"use strict";\n(function () {\n',
                    footer: '})()'
                },
                src: ['tmp/js/*.js', '!tmp/js/Game.js', 'tmp/js/Game.js'],
                dest: 'dist/js/memory.js'
            }
        },
        clean: {
            pre: ['dist/js/', 'dist/css/'],
            post: ['tmp/']
        },
        less: {
            build: {
                options: {
                    compress: false,
                    sourceMap: false
                },
                src: 'src/less/*.less',
                dest: 'dist/css/memory.css'
            }
        }
    })

    grunt.loadNpmTasks('grunt-babel')
    grunt.loadNpmTasks('grunt-contrib-concat')
    grunt.loadNpmTasks('grunt-contrib-clean')
    grunt.loadNpmTasks('grunt-contrib-less')

    grunt.registerTask('default', ['clean:pre','babel','less','concat','clean:post'])
}
