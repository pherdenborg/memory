const express = require('express')
const app = express()
const http = require('http')
const port = 8111

app.use(express.static('dist'));

app.get('/colors', (req, res) => {
    const buf = []
    http.request('http://worktest.accedo.tv/colours.conf', (response) => {
        response
        .on('data', (part) => buf.push(part))
        .on('end', () => res.send(buf.join('')))
    })
    .end()
})

app.listen(port, function () {
  console.log(`Peter's Memory game app listening on port ${port}.`);
});

