const Timer = (callback, interval) => {
    let time,
        timeout

    const start = () => {
        time = 0
        timeout = setInterval(() => {
            time += interval
            callback(time)
        }, interval)
    },

    stop = () => {
        clearTimeout(timeout)
    }

    return {
        start,
        stop
    }
}
