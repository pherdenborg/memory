const GameState = () => {
    const state = {},
    gameStates = {
        chooseFirstCard: 1,
        chooseSecondCard: 2,
        animating: 3,
        over: 4,
        modalDialogShowing: 5
    },

    set = (key, data) => {
        state[key] = data
    },

    setProp = (key, prop, data) => {
        if (state[key] && $.isPlainObject(state[key]))
            state[key][prop] = data
    },

    get = (key) => state[key],

    getProp = (key, prop) => {
        if (state[key] && $.isPlainObject(state[key]))
            return state[key][prop]
        return undefined
    },

    has = (key) => key in state,

    isSuspended = () => get('gameState') == gameStates.animating || get('gameState') == gameStates.modalDialogShowing

    return {
        get,
        getProp,
        set,
        setProp,
        has,
        isSuspended,
        getGameStates: () => gameStates
    }
}

