const EventHandler = ($root, game, board) => {

    const keyMap = {
        9: 'tab',
        13: 'enter',
        32: 'space',
        37: 'left',
        38: 'up',
        39: 'right',
        40: 'down'
    },
    state = game.getState()

    return {
        listen: () => {
            $root
            .off('keyup click')
            .on('keyup', (event) => {
                if (state.isSuspended()) {
                    return
                }
                switch (keyMap[event.which]) {
                    case 'up':
                        board.moveUp()
                        break;
                    case 'tab':
                    case 'right':
                        board.moveRight()
                        break;
                    case 'down':
                        board.moveDown()
                        break;
                    case 'left':
                        board.moveLeft()
                        break;
                    case 'enter':
                    case 'space':
                        board.triggerAction()
                        break;
                }
                event.preventDefault()
            })
            .on('click', '.boardItem', (event) => {
                if (state.isSuspended()) {
                    return
                }
                board.handleClick($(event.target))
            })
        }
    }
}
