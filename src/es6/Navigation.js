const Navigation = (launchButton, rows, cols) => {

    let cards

    const setCards = (newCards) => {
        cards = newCards
    },

    moveUp = (focussed) => {
        let cardAbove
        if (focussed.getIndex() === undefined)
            return;
        if ((focussed.getIndex() + 1) / cols > 1)
            cardAbove = getClosestCard(focussed.getIndex() - cols, 'up');
        (cardAbove || launchButton).focus()
    },

    moveRight = (focussed) => {
        let cardToRight
        if (focussed.getIndex() === undefined || (focussed.getIndex() + 1) % cols == 0)
            return;
        cardToRight = getClosestCard(focussed.getIndex() + 1, 'right')
        if (cardToRight)
            cardToRight.focus()
    },

    moveDown = (focussed) => {
        let cardBelow
        if ((focussed.getIndex() !== undefined && focussed.getIndex() + 1) / cols > 1)
            return;
        cardBelow = getClosestCard(focussed.getIndex() == undefined ? Math.floor(cols / 2 - 1) : focussed.getIndex() + cols , 'down')
        if (cardBelow)
            cardBelow.focus()
    },

    moveLeft = (focussed) => {
        let cardToLeft
        if (focussed.getIndex() === undefined || (focussed.getIndex() + 1) % cols == 1)
            return;
        cardToLeft = getClosestCard(focussed.getIndex() - 1, 'left')
        if (cardToLeft)
            cardToLeft.focus()
    },

    getClosestCard = (index, direction) => {
        let candidate = cards[index],
            newIndex = index,
            row = Math.floor(index / cols) + 1,
            col = index % cols + 1

        while ((!candidate || candidate.getCardState() == 'removed') &&
           row > 0 && row <= rows &&
           col > 0 && col <= cols &&
           !(col == cols && direction == 'left') &&
           !(col == 1 && direction == 'right')) {
            switch (direction) {
                case 'up':
                    candidate = getClosestInRow(newIndex)
                    newIndex -= cols
                    break;
                case 'right':
                    candidate = getClosestInCol(newIndex)
                    newIndex += 1
                    break;
                case 'down':
                    candidate = getClosestInRow(newIndex)
                    newIndex += cols
                    break;
                case 'left':
                    candidate = getClosestInCol(newIndex)
                    newIndex -= 1
                    break;
            }
            row = Math.floor(newIndex / cols) + 1
            col = newIndex % cols + 1
        }
        return !candidate || candidate.getCardState() == 'removed' ? null : candidate
    },

    getClosestInRow = (index) => {
        const col = index % cols + 1
        let offset = 0
        while (col - offset > 0 || col + offset <= cols) {
            if (col - offset > 0 && cards[index - offset].getCardState() != 'removed')
                return cards[index - offset]
            if (offset > 0 && col + offset <= cols && cards[index + offset].getCardState() != 'removed')
                return cards[index + offset]
            offset++
        }
        return null
    },

    getClosestInCol = (index) => {
        const row = Math.floor(index / cols) + 1
        let offset = 0
        while (row - offset > 0 || row + offset <= rows) {
            if (row - offset > 0 && cards[index - offset * cols].getCardState() != 'removed') {
                return cards[index - offset * cols]
            }
            if (offset > 0 && row + offset <= rows && cards[index + offset * cols].getCardState() != 'removed') {
                return cards[index + offset * cols]
            }
            offset++
        }
        return null
    },

    getFirstRemainingCard = () => cards.find((card) => card.getCardState() != 'removed')

    return {
        setCards,
        moveUp,
        moveRight,
        moveDown,
        moveLeft,
        getFirstRemainingCard
    }
}
