const Board = (game, rows, cols, $gameArea) => {

    let cards,
        cardsLeft,
        colors,
        launchButton,
        focussed,
        cardUp,
        navigation

    const state = game.getState(),

    setColors = (newColors) => {
        colors = newColors
    },

    boardItem = (state) => ({
        getElement: () => state.element,
        blur: () => {
             state.element.removeClass('focussed')
             state.elemSpecs.class = state.elemSpecs.class.replace(' focussed', '')
             state.focussed = false
        },
        focus: () => {
            if (focussed) {
                focussed.blur()
            }
            if (state.index == undefined)
                focussed = launchButton
            else {
                focussed = cards[state.index]
                state.elemSpecs.class += ' focussed'
            }
            if (state.element)
                state.element.addClass('focussed')
        },
        getIndex: () => state.index,
        isRendered: () => state.rendered,
        renderTo: ($target) => {
            state.element = $('<div/>', state.elemSpecs)
            .appendTo($target)
            state.rendered = true
        }
    }),

    card = (color, index) => {
        const state = {
            element: null,
            color,
            index,
            cardState: 'down',
            elemSpecs: {
                class: 'boardItem card',
                css: {
                    backgroundColor: color
                },
                data: {
                    index
                }
            },
            rendered: false
        }
        return Object.assign(boardItem(state), {
            getColor: () => state.color,
            getCardState: () => state.cardState,
            flip: () => {
                if (state.cardState == 'down') {
                    state.element.addClass('up')
                    state.cardState = 'up'
                }
                else {
                    state.element.removeClass('up')
                    state.cardState = 'down'
                }
            },
            indicateMatch: (matching, callback) => {
                const $indicator = $('<div />', {
                    class: 'indicator',
                    html: matching ? '\u2713' : '\u274c'
                })
                let animationIterations = 0
                state.element
                .append($indicator)
                .addClass(matching ? 'matching' : 'mismatching')
                state.element.on('animationiteration', () => {
                    console.log('caught animationiteration')
                    if (++animationIterations == 2) {
                        $indicator.remove()
                        if (matching) {
                            state.element
                            .removeClass('matching')
                            .addClass('removed')
                            state.cardState = 'removed'
                        }
                        else {
                            state.element.removeClass('up mismatching')
                            state.cardState = 'down'
                        }
                        if (callback) {
                            callback()
                        }
                    }
                })
            }
        })
    },

    createLaunchButton = () => {
        const state = {
            label: 'Restart',
            element: null,
            elemSpecs: {
                text: 'Restart',
                id: 'launchButton',
                class: 'boardItem'
            },
            rendered: false
        }
        return Object.assign(boardItem(state), {
            label: 'Restart',
            setLabel: (newLabel) => {
                state.label = newLabel
                state.element.text(state.label)
            }
        })
    },

    // Thanks https://bost.ocks.org/mike/shuffle/ for the randomizing algorithm
    getRandomCardArray = (arr) => {
        const array = arr
            .concat(arr)
            .slice()
        let pos = array.length,
            temp,
            swapPos
            while (pos) {
                swapPos = Math.floor(Math.random() * pos--)
                temp = array[pos]
                array[pos] = array[swapPos]
                array[swapPos] = temp
            }
            return array
    },

    getGameState = () => gameState,

    render = () => {
        if (!launchButton.isRendered())
            launchButton.renderTo($gameArea)
        cards.forEach((card) => card.renderTo($gameArea))
    },

    triggerAction = () => {
        let matching
        if (focussed === launchButton)
            game.end('restart')
        else {
            switch (state.get('gameState')) {
                case state.getGameStates().chooseFirstCard:
                    if (focussed.getCardState() == 'up')
                        return;
                    focussed.flip()
                    cardUp = focussed
                    state.set('gameState', state.getGameStates().chooseSecondCard)
                    break;
                case state.getGameStates().chooseSecondCard:
                    if (focussed.getCardState() == 'up')
                        return;
                    focussed.flip()
                    matching = cardUp.getColor() == focussed.getColor()
                    if (matching)
                        cardsLeft -= 2
                    state.set('gameState', state.getGameStates().animating)
                    cardUp.indicateMatch(matching, () => {
                        if (cardsLeft) {
                            state.set('gameState', state.getGameStates().chooseFirstCard)
                            if (matching) {
                                game.registerMatch()
                                navigation.getFirstRemainingCard().focus()
                            }
                            else {
                                 game.registerMiss()
                            }
                        }
                        else {
                             launchButton.focus()
                             game.end('completed')
                        }
                    })
                    focussed.indicateMatch(matching)
                    break;
            }
        }
    },

    handleClick = ($boardItem) => {
        if ($boardItem.data('index') == undefined)
            launchButton.focus()
        else
            cards[$boardItem.data('index')].focus()
        triggerAction()
    },

    setupCards = () => {
        cards = getRandomCardArray(colors)
            .map(card)
        state.set('gameState', state.getGameStates().chooseFirstCard)
        cardsLeft = rows * cols
        render()
        navigation.setCards(cards)
        navigation.getFirstRemainingCard().focus()
    },

    initialize = () => {
        launchButton = createLaunchButton()
        navigation = Navigation(launchButton, rows, cols)
    }

    initialize()

    return {
        getGameState,
        render,
        triggerAction,
        handleClick,
        setColors,
        setupCards,
        moveUp: () => navigation.moveUp(focussed),
        moveRight: () => navigation.moveRight(focussed),
        moveDown: () => navigation.moveDown(focussed),
        moveLeft: () => navigation.moveLeft(focussed)
    }
}
