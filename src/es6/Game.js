/*
 * The Game module is the main entry point of the application and is responsible
 * for initializing other modules, displaying non-game board specific contents such
 * as statistics and for responding to various high-level game events.
 */
const Game = () => {

    let board,
        eventHandler

    const rows = 2,
        cols = 8,
        $gameArea = $('#gameArea'),
        $sessionStateArea = $('#sessionState'),
        $statsArea = $('#stats'),
        interval = 1000,

        restart = () => {
             clearGameArea()
             startNewGame()
        },

        /* Remove the non-static elements, i.e. the cards  */
        clearGameArea = () => {
            $gameArea
                .find('.card')
                .remove()
        },

        /* Update and redraw statistics, display sucess dialog  */
        registerSession = (type) => {
            const stats = state.get('stats')
            let timeRecord = false,
                scoreRecord = false
            timer.stop()
            if (state.get('gameState') != state.getGameStates().over)
                stats.played++
            if (type == 'win') {
                stats.won++
                if (state.getProp('sessionState', 'elapsed') < stats.fastest) {
                     stats.fastest = state.getProp('sessionState', 'elapsed')
                     timeRecord = true
                }
                if (state.getProp('sessionState','score') > stats.highscore) {
                     stats.highscore = state.getProp('sessionState','score')
                     scoreRecord = true
                }
            }
            else {
                 state.set('sessionState', getDefaultSessionState())
            }
            state.set('gameState', state.getGameStates().over)
            state.set('stats', stats)
            renderSessionState()
            renderStats()
            if (type == 'win')
                displaySuccessDialog(timeRecord, scoreRecord)
        },

        /* Update session time and redraw session stats  */
        updateSessionTime = (elapsed) => {
            state.setProp('sessionState', 'elapsed', elapsed / interval)
            renderSessionState()
        },

        registerMatch = () => registerAttempt(true),

        registerMiss = () => registerAttempt(false),

        /* Update score and re-draw session stats  */
        registerAttempt = (match) => {
            const sessionState = state.get('sessionState')
            if (match) {
                sessionState.cardsLeft++
                sessionState.score += 5
            }
            else
                sessionState.score -= 1
            state.set('sessionState', sessionState)
            renderSessionState()
        },

        /* Render the session state  */
        renderSessionState = () => {
            const sessionState = state.get('sessionState')
            $sessionStateArea.html(
                `<div class="infoPair"><label>Time elapsed</label><span class="value">${formatTime(sessionState.elapsed)}</span></div>` +
                `<div class="infoPair"><label>Cards left</label><span class="value">${sessionState.cardsLeft}</span></div>` +
                `<div class="infoPair"><label>Score</label><span class="value">${sessionState.score}</span></div>`
            )
        },

        /* Render long-term stats  */
        renderStats = () => {
            const stats = state.get('stats')
            $statsArea.html(
                `<div class="infoPair"><label>Games played</label><span class="value">${stats.played}</span></div>
                <div class="infoPair"><label>Games won</label><span class="value">${stats.won}</span></div>
                ${stats.fastest < 99999 ? `<div class="infoPair"><label>Fastest game</label><span class="value">${formatTime(stats.fastest)}</span></div>` : ''}
                ${stats.highscore > -99999 ? `<div class="infoPair"><label>Highscore</label><span class="value">${stats.highscore}</span></div>` : ''}`
            )
        },

        /* Handle ending of a game  */
        end = (reason) => {
            switch (reason) {
                case 'restart':
                    registerSession('loss')
                    restart()
                    break
                case 'completed':
                    state.setProp('sessionState', 'cardsLeft', 0)
                    registerSession('win')
                    break
            }
        },

        timer = Timer(updateSessionTime, interval),

        state = GameState(),

        getDefaultSessionState = () => ({
            elapsed: 0,
            cardsLeft: rows * cols,
            score: 0
        }),

        startNewGame = () => {
            state.set('sessionState', getDefaultSessionState())
            state.set('gameState', state.getGameStates().chooseFirstCard)
            board.setupCards()
            timer.start()
            renderSessionState()
            renderStats()
        },

        initialize = () => {
            state.set('stats', {
                played: 0,
                won: 0,
                fastest: 99999,
                highscore: -99999
            })
            board = Board(self, rows, cols, $gameArea)
            eventHandler = EventHandler($(document), self, board)
            $.get('/colors')
                .fail(() => console.error('could not fetch colors'))
                .done((data) => {
                    const colors = data
                        .split(/(\r\n|\r|\n)/)
                        .filter((line) => line.length && line.charAt(0) == '#')
                        .map((line) => line.replace(/(\S*).*/, '$1'))
                    board.setColors(colors)
                    eventHandler.listen()
                    startNewGame()
                })
        },

        /* Format time as m:ss */
        formatTime = (time) => {
            const minutes = Math.floor(time / 60),
                seconds = time % 60
            return `${minutes}:${seconds < 10 ? `0${seconds}` : seconds}`
        },

        /* Display a modal dialog with options to play again or quit  */
        displaySuccessDialog = (timeRecord, scoreRecord) => {
            state.set('gameState', state.getGameStates().modalDialogShowing)
            $('<div />', {
                id: 'mask'
            })
            .appendTo('body')
            $('<div />', {
                id: 'successDialog',
                html: `<p>Congratulations, you found all pairs!</p>
                    ${timeRecord ? `<p>You set a new time record of ${formatTime(state.getProp('stats', 'fastest'))}</p>` : ''}
                    ${scoreRecord ? `<p>You set a new highscore of ${state.getProp('stats', 'highscore')}</p>` : ''}
                    <input type="button" id="playAgainButton" value="Play again" />
                    <input type="button" id="quitButton" value="Quit" />`,
                css: {
                    opacity: 0.001
                }
            })
            .appendTo('body')
            .fadeTo(250, 1)
            .on('click', 'input', (event) => {
                if (event.target.id == 'playAgainButton')
                    restart()
                else {
                    clearGameArea()
                    state.set('sessionState', getDefaultSessionState())
                    state.set('gameState', state.getGameStates().chooseFirstCard)
                    renderSessionState()
                }
                hideSuccessDialog()
            })
            .find('#playAgainButton')
            .focus()
        },

        hideSuccessDialog = () => {
            $('body')
                .find('#mask, #successDialog')
                .fadeOut(() => {
                    $('body')
                    .find('#mask, #successDialog')
                    .hide()
                })
        },

        self = {
            initialize,
            end,
            registerMatch,
            registerMiss,
            getState: () => state
        }

        return self
}

Game().initialize()
